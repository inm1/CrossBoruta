#' Cross-Boruta analysis of multivariate interactions
#'
#' @param X Data frame with the set to be analysed.
#' @param targets If given, narrows the analysis to a given subset of \code{X}'s columns.
#' @param withTentative If true, the method includes features claimed tentative by Boruta together with confirmed; otherwise, they are omitted.
#' @param withNormImp If true, normalised importance scores are calculated.
#' @param withSpearman If true, Spearman correlations between each detected feature pair is calculated.
#' @param doTrace Verbosity level; 0 for silence.
#'  Passed downstream to the \code{Boruta} call.
#' @param ... Additional parameters passed down to the Boruta call and to the importance adapter.
#' @return A \code{data.frame} collecting the detected interactions.
#'  Column \code{from} indicates which feature is relevant to the prediction of feature \code{to}, with a confidence score \code{normHits}.
#'  Optionally, if \code{withNormImp} is set, \code{normImportance} marks the strength of interaction, according to the importance scorer passed to Boruta.
#'  This value is a median importance from the last 5 Boruta iterations, and normalised so that 0 is mapped to the median maximal shadow importance, while 1 to the importance of the target feature predicting itself alone.
#'  Moreover, if \code{withSpearman} is set, \code{Spearman} column contains the Spearman correlation between two \code{from} and \code{to}; if one of said features is not ordinal, the value is \code{NA}.
#'  This table can be directly passed to \code{ggraph} for convenient plotting.
#' @examples
#' CrossBoruta(iris)
#' @importFrom Boruta Boruta getImpRfZ
#' @importFrom stats median cor
#' @importFrom utils tail
#' @export 

CrossBoruta<-function(
  X,targets=names(X),
  withTentative=FALSE,withNormImp=FALSE,withSpearman=FALSE,
  doTrace=0,...){
 stopifnot(is.data.frame(X))
 stopifnot(all(targets%in%names(X)))
 ordMask<-sapply(X,function(x) is.numeric(x) || is.ordered(x))


 lapply(which(names(X)%in%targets),function(idx){
  if(doTrace>0) message("Analysing target ",names(X)[idx])

  #Executing Boruta & extracting useful outputs
  Boruta::Boruta(X[,-idx],X[,idx],doTrace=doTrace,holdHistory=TRUE,...)->B
  selMask<-with(B,if(withTentative) finalDecision!="Rejected" else finalDecision=="Confirmed")
  if(!any(selMask)) return(NULL)
  selImp<-B$ImpHistory[,c(selMask,rep(FALSE,3)),drop=FALSE]
  xshImp<-B$ImpHistory[,"shadowMax"]

  data.frame(
   from=colnames(selImp),
   to=names(X)[idx],
   normHits=colMeans(selImp>xshImp)
  )->ans

  #Importance normalisation, if desired
  ans$normImportance<-if(withNormImp){
   NRM_OVER<-5
   (function(getImp=Boruta::getImpRfZ) getImp)(...)(X[,idx,drop=FALSE],X[,idx])->tarImp
   xshAve<-median(tail(xshImp,NRM_OVER),na.rm=TRUE)
   (apply(tail(selImp,NRM_OVER),2,median,na.rm=TRUE)-xshAve)/(tarImp-xshAve)
  }

  #Spearman correlation, if desired
  ans$Spearman<-if(withSpearman)
   if(ordMask[idx]){
    sapply(ans$from,function(x) if(ordMask[x]) cor(X[,idx],X[,x],method="spearman",use="pair") else NA)
   }else NA

  ans
 })->ans
 do.call(rbind,ans)->ans
 rownames(ans)<-NULL
 class(ans)<-c("CrossBorutaOutput",class(ans))
 ans
}
